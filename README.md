# Vibe Bot

Invite link (Administrator): https://discord.com/api/oauth2/authorize?client_id=789722959567781907&permissions=8&scope=bot%20applications.commands

## Requirements

python3.9+

### Bot token

Create a `.env` file, and fill that file with the text `DISCORD_BOT_TOKEN=token123`, obviously replacing "token123" with whatever Discord bot account's token that you'll be using to test. Make sure to source .env.

Find your bot token by going to https://discord.com/developers/applications. Make a new application, and go to the bot settings of your application page. Make a bot for your application, and then copy the bot's token.

## Running vibebot (No Docker)

We use pipenv, which manages our installed packages and virtual environment at the same time. Note that pipenv automatically load your .env.

### 0. Get pipenv

`python -m pip install pipenv`

### 1. Install requirements

`pipenv install`

### 2. Start pipenv shell

`pipenv shell`

### 3. Launch the bot

`python vibebot.py`

## Running vibebot (Docker + VSCode)

### 0. Pre-Requisites

Local dev builds (and production builds) are handled via Docker.  To run the Docker container on your own system, download the [Docker Desktop](https://www.docker.com/products/docker-desktop) app.  Additionally, VSCode Docker configuration files are created and configured for use.  While convenient, using this is technically not required, although this explanation will assume its use.

### 1. Launch local environment

Load the project in VSCode, and run the dev environment through the built in debugger (or by pressing F5). Note that docker will automatically load your .env.

## Continuous Integration

When a new change is committed, gitlab will automagically build and deploy a container with the new changes. View CI status in gitlab's CI tab.

## Testing

We use pytest.

To run tests, type `pytest` on the root directory. Don't use pytest in the tests directory, because
of module pathing concerns.
