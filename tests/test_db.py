import pytest
import os

from exts.db import GuildDB

test_db = "_test"

class MockGuild:
    def __init__(self, id):
        self.id = id

@pytest.fixture
def guild():
    return MockGuild(123456)


@pytest.fixture
def guild2():
    return MockGuild(654321)


@pytest.fixture
def bot():
    # In the future we might need to mock up the bot object for GuildDB.
    return {}


@pytest.fixture
def db(bot):
    # For some reason it's possible for shelve to create "test" or "test.db" as the db file, so
    # we have to account for both.
    if os.path.exists(f"storage/{test_db}"):
        os.remove(f"storage/{test_db}")
    if os.path.exists(f"storage/{test_db}.db"):
        os.remove(f"storage/{test_db}.db")
    guild_db = GuildDB(bot, f"storage/{test_db}")
    yield guild_db
    guild_db.close()
    if os.path.exists(f"storage/{test_db}.db"):
        os.remove(f"storage/{test_db}.db")
    if os.path.exists(f"storage/{test_db}"):
        os.remove(f"storage/{test_db}")


def test_db_exists(db):
    assert os.path.exists(f"storage/{test_db}.db") or os.path.exists(f"storage/{test_db}")


def test_db_set_get(db, guild, guild2):
    db.set_value(guild.id, "foo", "hello")
    db.set_value(guild.id, "bar", "world")
    db.set_value(guild2.id, "foo", "hello from guild 2")
    db.set_value(guild2.id, "bar", "guild 2's world")

    assert db.get_value(guild.id, "foo") == "hello"
    assert db.get_value(guild.id, "bar") == "world"
    assert db.get_value(guild2.id, "foo") == "hello from guild 2"
    assert db.get_value(guild2.id, "bar") == "guild 2's world"

    assert db.get_value(guild.id, "this key doesn't exist") == None
    assert db.get_value(guild.id, "this key doesn't exist", "default") == "default"

    # Test replacing key
    db.set_value(guild.id, "foo", "hi")
    assert db.get_value(guild.id, "foo") == "hi"


def test_db_get_object_copy(db, guild):
    # Objects should be deep copied when pulled from the database, meaning if the pulled object
    # is changed, it doesn't change the one in the database.
    db.set_value(guild.id, "dummy", {"id": "unchanged"})
    dummy = db.get_value(guild.id, "dummy")
    assert dummy["id"] == "unchanged"

    dummy["id"] = "changed"
    assert db.get_value(guild.id, "dummy")["id"] == "unchanged"


def test_db_rm(db, guild):
    db.set_value(guild.id, "foo", "hi")
    db.set_value(guild.id, "bar", "bye")

    db.rm_value(guild.id, "foo")
    assert db.get_value(guild.id, "foo") == None
    assert str(guild.id) in db.db
    # Check that we can remove nonexistent key with no errors.
    db.rm_value(guild.id, "doesn't exist")

    db.rm_value(guild.id, "bar")
    assert db.get_value(guild.id, "bar") == None
    # Check that we removed the guild entirely from the db.
    assert not str(guild.id) in db.db

    # Check that we can remove key in nonexistent guild with no errors.
    db.rm_value(1231231123123123, "doesn't exist")
