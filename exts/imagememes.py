import io
from typing import List, Tuple

from PIL import Image

import discord
from discord.ext import commands
from discord import app_commands

class AvatarInfo():
    def __init__(self, member: discord.Member, coords: Tuple[int, int], size: Tuple[int, int]):
        self.member = member
        self.coords = coords
        self.size = size


class ImageMemes(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    async def image_meme(self, template_file: str, avatarinfos: List[AvatarInfo]):
        template = Image.open(template_file)

        for ai in avatarinfos:
            avatar_bytes = io.BytesIO()
            await ai.member.avatar_url.save(avatar_bytes, seek_begin=True)
            avatar = Image.open(avatar_bytes)

            avatar = avatar.resize(ai.size)

            template.paste(avatar, ai.coords, avatar.convert("RGBA"))

        meme = io.BytesIO()
        template.save(meme, format="PNG")
        meme.seek(0)
        return discord.File(meme, "meme.png")

    @app_commands.command(name="spank", description="Spank yo friends")
    @app_commands.describe(spanker="The spanker.", spankee="The one getting spanked.")
    async def spank(self, interaction: discord.Interaction, spanker: discord.User, spankee: discord.User):
        await interaction.defer()
        avatars = [
            AvatarInfo(spanker, (310, 40), (128, 128)),
            AvatarInfo(spankee, (460, 290), (128, 128))
        ]
        meme_file = await self.image_meme("resources/images/spank.jpg", avatars)
        await interaction.response.send_message(file=meme_file)

    @app_commands.command(name="hornybonk", description="Go to horny jail!")
    @app_commands.describe(bonked="The one getting bonked.", bonker="The bonker.")
    async def hornybonk(self, interaction: discord.Interaction, bonked: discord.User, bonker: discord.User = None):
        await interaction.defer()
        if not bonker:
            avatars = [
                AvatarInfo(bonked, (443, 241), (128, 128))
            ]
        else:
            avatars = [
                AvatarInfo(bonker, (168, 91), (128, 128)),
                AvatarInfo(bonked, (443, 241), (128, 128))
            ]
        meme_file = await self.image_meme("resources/images/hornybonk.png", avatars)
        await interaction.response.send_message(file=meme_file)

async def setup(bot: commands.Bot):
    await bot.add_cog(ImageMemes(bot))
