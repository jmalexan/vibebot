import os
import shelve
import copy

import discord
from discord.ext import commands

class GuildDB(commands.Cog):
    def __init__(self, bot: commands.Bot, location: str):
        self.bot = bot
        os.makedirs(os.path.dirname(location), exist_ok=True)
        self.db = shelve.open(location, writeback=True)

    def cog_unload(self):
        self.close()

    def get_value(self, guild: int, key: str, default=None):
        if str(guild) in self.db and key in self.db[str(guild)]:
            return copy.deepcopy(self.db[str(guild)][key])
        else:
            return default

    def set_value(self, guild: int, key: str, value):
        if not str(guild) in self.db:
            self.db[str(guild)] = {}
        self.db[str(guild)][key] = value
        self.db.sync()

    def rm_value(self, guild: int, key: str):
        guild = str(guild)
        if not guild in self.db or not key in self.db[guild]:
            return
        # Remove key from guild.
        del self.db[guild][key]
        # If guild is empty, remove guild.
        if not self.db[guild]:
            del self.db[guild]
        self.db.sync()

    def close(self):
        self.db.close()

async def setup(bot: commands.Bot):
    await bot.add_cog(GuildDB(bot, "storage/shelf"))
