import random
from exts.db import GuildDB
import discord
from discord.ext import commands
from discord import app_commands

class ExtendoChannels(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db: GuildDB = bot.get_cog("GuildDB")

    # @commands.Cog.listener()
    # def on_ready(self):
    #     # Purge any extra extendochannels
    #     for guild in self.bot.guilds:
    #         ecprefix = self.db.get_value(guild.id, "ecprefix", None)
    #         if ecprefix == None:
    #             continue
    #         guild.voice_channels

    @commands.Cog.listener()
    async def on_voice_state_update(self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
        # Create new EC and/or delete empty EC
        if before.channel == after.channel:
            return

        if before.channel != None:
            beforeecprefix = self.db.get_value(before.channel.guild.id, "ecprefix", None)
            if beforeecprefix != None and len(before.channel.members) == 0 and before.channel.name.startswith(beforeecprefix):
                await before.channel.delete()

        if after.channel != None:
            afterecprefix = self.db.get_value(after.channel.guild.id, "ecprefix", None)
            if afterecprefix != None and len(after.channel.members) == 1 and after.channel.name.startswith(afterecprefix):
                afterecnames = self.db.get_value(after.channel.guild.id, "ecdefaultname", ["Voice Channel"])

                # Temporary fix to convert old string names to list
                if isinstance(afterecnames, str):
                    afterecnames = [afterecnames]
                    self.db.set_value(after.channel.guild.id, "ecdefaultname", afterecnames) 
                
                chosenname = random.choice(afterecnames)
                await after.channel.guild.create_voice_channel(f"{afterecprefix} {chosenname}", category=after.channel.category)

    @app_commands.command(name="ecprefix", description="Change the prefix for extendo channel names.")
    @app_commands.describe(prefix="The prefix to use for extendo channels.  Default is `🎮`")
    @app_commands.default_permissions()
    @app_commands.guild_only()
    async def ecprefix(self, interaction: discord.Interaction, prefix: str):
        # Set prefix for ECs
        self.db.set_value(interaction.guild.id, "ecprefix", prefix)
        await interaction.response.send_message(f"Updated Expando-Channel prefix to `{prefix}`!")

    @app_commands.command(name="ecdefaultname", description="Change the default name for extendo channels.")
    @app_commands.describe(names="The default name to use for extendo channels.  Default is `Voice Channel`.  If a list is provided (separated by commas), a random name will be chosen from the list.")
    @app_commands.default_permissions()
    @app_commands.guild_only()
    async def ecdefaultname(self, interaction: discord.Interaction, names: str):
        # Set default name for ECs
        names = names.split(',')
        names = list(map(lambda x: x.strip(), names))
        self.db.set_value(interaction.guild.id, "ecdefaultname", names)
        await interaction.response.send_message(f"Updated Expando-Channel default name to `{names}`!", ephemeral=True)

    @app_commands.command(name="ecname", description="Change the name of the extendo channel you're currently in.")
    @app_commands.describe(name="The name to use for your extendo channel.")
    @app_commands.guild_only()
    async def ecname(self, interaction: discord.Interaction, name: str):
        # Set EC name if user is in one
        ecprefix = self.db.get_value(interaction.guild.id, "ecprefix", None)
        if ecprefix != None:
            if interaction.user.voice.channel != None and interaction.user.voice.channel.name.startswith(ecprefix): 
                if len(name) > 0:
                    await interaction.user.voice.channel.edit(name=f"{ecprefix} {name}")
                    await interaction.response.send_message(f"Expando-Channel name set to `{ecprefix} {name}`!", ephemeral=True)
                else:
                    await interaction.response.send_message("No name detected, please enter a name for the Expando-Channel!", ephemeral=True)
            else:
                await interaction.response.send_message(f"This command can only be run when in an Expando-Channel, look for a voice channel that starts with `{ecprefix}`!", ephemeral=True)
        else:
            await interaction.response.send_message("This server doesn't have Expando-Channels enabled!  Ask an admin to enable it.", ephemeral=True)

    @app_commands.command(name="eclimit", description="Limit the number of people who can join the extendo channel you're currently in.")
    @app_commands.describe(limit="The maximum number of people who will be able to join your extendo channel.")
    @app_commands.guild_only()
    async def eclimit(self, interaction: discord.Interaction, limit: int):
        # Set EC member limit
        ecprefix = self.db.get_value(interaction.guild.id, "ecprefix", None)
        if ecprefix != None:
            if interaction.user.voice.channel != None and interaction.user.voice.channel.name.startswith(ecprefix): 
                if limit != None and limit > 0:
                    await interaction.user.voice.channel.edit(user_limit=limit)
                    await interaction.response.send_message(f"Expando-Channel user limit set to {limit}!", ephemeral=True)
                else:
                    await interaction.user.voice.channel.edit(user_limit=0)
                    await interaction.response.send_message("Missing or invalid user limit detected, set to no user limit.", ephemeral=True)
            else:
                await interaction.response.send_message(f"This command can only be run when in an Expando-Channel, look for a voice channel that starts with `{ecprefix}`!", ephemeral=True)
        else:
            await interaction.response.send_message("This server doesn't have Expando-Channels enabled!  Ask an admin to enable it.", ephemeral=True)

async def setup(bot: commands.Bot):
    await bot.add_cog(ExtendoChannels(bot))