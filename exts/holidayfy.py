import random

import discord
from discord.ext import commands
from discord import app_commands

from exts.db import GuildDB


class Holidayfy(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db: GuildDB = bot.get_cog("GuildDB")
        
    def get_channels(self, interaction: discord.Interaction):
        # Excluding voice because it does not work well with expando # channels.
        # Assumption is made here that guild channel is never private or group. So this 
        # includes text, category, news, store, which are apparently valid guild channels.
        return [c for c in interaction.guild.channels if c.type != discord.ChannelType.voice]

    def get_random_emoji(self, symbols: str):
        return symbols[random.randrange(0, len(symbols))]

    @app_commands.command(name="holidayfy", description="Add your decorations.")
    @app_commands.describe(symbol="It can be an emoji! (But not a custom emote, please!)")
    @app_commands.default_permissions()
    @app_commands.guild_only()
    async def add(self, interaction: discord.Interaction, symbol: str = None):
        await interaction.defer()
        channels = self.get_channels(interaction)
        # The first send uses content= so that slash command can respond with a source ack.
        await interaction.response.send_message(content=f"Vibing up channels with {symbol}! Please note that I might get rate limited!", ephemeral=True)
        for channel in channels:
            emoji = self.get_random_emoji(symbol)
            new_name = f"{emoji}{channel.name}{emoji}"
            await channel.edit(name=new_name)
        self.db.set_value(ctx.guild.id, "holiday_text", symbol)
        # The second send uses the channel because discord_slash can't handle two sends in one command.
        await interaction.response.send_message(content="Done! Look at all that vibe!", ephemeral=True)

    @app_commands.command(name="deholidayfy", description="Remove your decorations.")
    @app_commands.describe(symbol="If you don't give me a symbol, I will use the one you added last")
    @app_commands.default_permissions()
    @app_commands.guild_only()
    async def remove(self, interaction: discord.Interaction, symbol: str = None):
        await interaction.defer()
        if not symbol:
            # If a text is not specified, use one that's saved.
            symbol = self.db.get_value(interaction.guild.id, "holiday_text")
            if not symbol:
                await interaction.response.send_message("I don't have the holiday emote saved. Please specify one!", ephemeral=True)
                return
        # The first send uses content= so that slash command can respond with a source ack.
        await interaction.response.send_message(content=f"Getting rid of {symbol} from channels!", ephemeral=True)
        channels = self.get_channels(interaction)
        for channel in channels:
            new_name = channel.name
            if new_name[0] in symbol:
                new_name = new_name[1:]
            if new_name[-1] in symbol:
                new_name = new_name[:-1]
            await channel.edit(name=new_name)
        self.db.rm_value(interaction.guild.id, "holiday_text")
        # The second send uses the channel because discord_slash can't handle two sends in one command.
        await interaction.response.send_message(content="Done!", ephemeral=True)


async def setup(bot: commands.Bot):
    await bot.add_cog(Holidayfy(bot))
