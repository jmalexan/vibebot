import discord
from discord import app_commands
from discord.ext import commands


class Basic(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @app_commands.command(name="ping", description="Hi")
    async def ping(self, interaction: discord.Interaction):
        await interaction.response.send_message(content="I just kinda vibe I guess?")

async def setup(bot: commands.Bot):
    await bot.add_cog(Basic(bot))
