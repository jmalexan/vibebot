import asyncio
import datetime
import time
from uuid import uuid4
from exts.db import GuildDB
import discord
from discord.ext import commands
from discord import app_commands

AURORA_ID = 732433259828609095
AURORA_OBJECT = discord.Object(id=AURORA_ID)
NOMINATION_CHAN = discord.Object(id=782352273477074975)
CLAN_RULES_CHAN = discord.Object(id=734809757088612483)
NOTDISLIKED_ID = 137429565063692289

class AuroraNomination(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db: GuildDB = bot.get_cog("GuildDB")

        nominations = self.db.get_value(AURORA_ID, "nominations", [])
        for nom in nominations:
            asyncio.create_task(self.wait_for_nom(nom))
            pass

    @commands.Cog.listener()
    async def on_ready(self):
        self.bot.add_view(Nomination(self.db))

    @app_commands.command(name="nominate", description="Nominate a new member in Aurora.")
    @app_commands.describe(nominee="The name of the person who is being nominated", nominator="The person who is nominating the nominee", everyone_ping="Whether to ping @everyone in the nomination message or not")
    @app_commands.guilds(AURORA_OBJECT)
    @app_commands.default_permissions()
    async def nominate(self, interaction: discord.Interaction, nominee: str, nominator: discord.Member, everyone_ping: bool = True) -> None:
        await interaction.response.send_message("Nominating...", ephemeral=True)
        nomination_channel = await self.bot.fetch_channel(NOMINATION_CHAN.id)
        nom_message_text = ""
        if everyone_ping:
            nom_message_text += "@everyone\n"
        nom_message_text += f"{nominator.mention} has nominated {nominee}.  Click Reject to reject the nomination.  After 72 hours, {nominee} will be admitted into the clan."
        nomination_message = await nomination_channel.send(nom_message_text, view=Nomination(self.db))
        nominations = self.db.get_value(AURORA_ID, "nominations", [])
        nom = {
            "nominee": nominee,
            "nominator": nominator.id,
            "message": nomination_message.id,
            "start_time": time.time()
        }

        nominations.append(nom)
        self.db.set_value(AURORA_ID, "nominations", nominations)
        asyncio.create_task(self.wait_for_nom(nom))

    async def wait_for_nom(self, nomination):
        await asyncio.sleep((nomination["start_time"] + (72 * 60 * 60)) - time.time())
        try:
            nom = next(filter(lambda x: x["message"] == nomination["message"], self.db.get_value(AURORA_ID, "nominations", [])))
        except StopIteration:
            return
        nomination_channel = await self.bot.fetch_channel(NOMINATION_CHAN.id)
        try:
            nomination_message = await nomination_channel.fetch_message(nomination["message"])
        except discord.errors.NotFound:
            andy_dms = await self.bot.fetch_user(NOTDISLIKED_ID)
            await andy_dms.send(f"Could not find the nomination message for {nomination['nominee']}.  This shouldn't happen.  Please investigate.")
            noms = list(filter(lambda x: x["message"] != nomination["message"], self.db.get_value(AURORA_ID, "nominations", [])))
            self.db.set_value(AURORA_ID, "nominations", noms)
            return
        await nomination_message.delete()
        clan_rules_chan = await self.bot.fetch_channel(CLAN_RULES_CHAN.id)
        invite = await clan_rules_chan.create_invite(max_age=0, max_uses=1, unique=True)
        nominator_dms = await self.bot.fetch_user(nomination["nominator"])
        nominator_dm_success = True
        try:
            await nominator_dms.send(f"Congratulations, {nomination['nominee']} was successfully admitted into the clan!  Here's an invite to send to them at your leisure: {invite.url}")
        except discord.errors.Forbidden: 
            nominator_dm_success = False
        andy_dms = await self.bot.fetch_user(NOTDISLIKED_ID)
        andy_msg = f"{nomination['nominee']} was automatically admitted, deleted the message."
        if nominator_dm_success:
            andy_msg += f" Sent an invite to {nominator_dms.mention}, but here's a copy just in case: {invite.url}"
        else:
            andy_msg += f" {nominator_dms.mention} has their DMs closed to me so I couldn't message them, please give them the invite: {invite.url}"
        await andy_dms.send(andy_msg)
        noms = list(filter(lambda x: x["message"] != nomination["message"], self.db.get_value(AURORA_ID, "nominations", [])))
        self.db.set_value(AURORA_ID, "nominations", noms)

class Nomination(discord.ui.View):
    def __init__(self, db: GuildDB):
        self.db = db
        super().__init__(timeout=None)

    @discord.ui.button(label='Reject', style=discord.ButtonStyle.danger, custom_id='nomination:reject')
    async def reject(self, interaction: discord.Interaction, button: discord.ui.Button):
        nom = get_nomination(interaction.message.id, self.db)
        if nom == None:
            await interaction.response.send_message("This nomination has already been processed, this shouldn't happen.", ephemeral=True)
            return
        await interaction.response.send_message(f"Are you sure you want to reject {nom['nominee']}?  If you click Reject, even as a joke, I will treat it as serious and the person will be rejected.", ephemeral=True, view=ConfirmRejection(self.db, interaction.message.id))

class ConfirmRejection(discord.ui.View):
    def __init__(self, db: GuildDB, nom_msg_id: int):
        self.db = db
        self.nom_msg_id = nom_msg_id
        super().__init__(timeout=180)

    @discord.ui.button(label='Reject', style=discord.ButtonStyle.danger, custom_id='confirmrejection:reject')
    async def reject(self, interaction: discord.Interaction, button: discord.ui.Button):
        await interaction.response.defer()
        nom = get_nomination(self.nom_msg_id, self.db)
        await interaction.client.get_channel(NOMINATION_CHAN.id).get_partial_message(self.nom_msg_id).delete()
        noms = list(filter(lambda x: x["message"] != nom["message"], self.db.get_value(AURORA_ID, "nominations", [])))
        self.db.set_value(AURORA_ID, "nominations", noms)
        await interaction.edit_original_response(content=f"You have successfully rejected the nomination of {nom['nominee']}.", view=None)
        await interaction.client.get_user(NOTDISLIKED_ID).send(f"{nom['nominee']} was rejected, deleted the message and let the nominator know.")
        await interaction.client.get_user(nom["nominator"]).send(f"Apologies, {nom['nominee']} was rejected and will not be admitted to the clan.  Feel free to reach out to NotDisliked with any questions, but know that rejections are completely anonymous, and so far no one has been rejected by mistake.")


    @discord.ui.button(label="Cancel", style=discord.ButtonStyle.secondary, custom_id="confirmrejection:cancel")
    async def cancel(self, interaction: discord.Interaction, button: discord.ui.Button):
        await interaction.response.defer()
        await interaction.delete_original_response()

def get_nomination(nom_msg_id: int, db: GuildDB):
    print(db.get_value(AURORA_ID, "nominations", []))
    nom = filter(lambda x: x["message"] == nom_msg_id, db.get_value(AURORA_ID, "nominations", []))
    try:
        nom = next(nom)
    except StopIteration:
        print("weird, bad")
        return
    return nom

async def setup(bot: commands.Bot):
    await bot.add_cog(AuroraNomination(bot))