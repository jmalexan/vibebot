import asyncio
import os
import logging

import discord
from discord.ext import commands

BOT_TOKEN = os.environ.get("DISCORD_BOT_TOKEN")
OWNER_DICT = {'Tin': '127564963270098944', 'Jon': '137429565063692289', 'Jaina': '167460739764846592'}
OWNER_IDS = set(OWNER_DICT.keys())

TEST_GUILD = discord.Object(id=505468672916193311)
AURORA_ID = 732433259828609095
AURORA_OBJECT = discord.Object(id=AURORA_ID)


# Discordpy will tell us if we're being rate limited.
logging.basicConfig(level=logging.WARNING)  

ext_list = ["exts.db", 
            "exts.basic", 
            "exts.extendochannels", 
            "exts.imagememes", 
            "exts.holidayfy",
            "exts.gibtwab",
            "exts.auroranominations",]

bot = commands.Bot(command_prefix="!", owner_ids=OWNER_IDS, intents=discord.Intents.all())

print("Starting...")

@bot.event
async def on_ready():
    print("Ready!")

async def setup_hook():
    # This copies the global commands over to your guild.
    bot.tree.copy_global_to(guild=TEST_GUILD)
    await bot.tree.sync(guild=TEST_GUILD)
    await bot.tree.sync(guild=AURORA_OBJECT)
    await bot.tree.sync()

bot.setup_hook = setup_hook

async def load():
    for ext in ext_list:
        await bot.load_extension(ext)

async def main():
    async with bot:
        await load()
        await bot.start(BOT_TOKEN)

if __name__ == '__main__':
    asyncio.run(main())